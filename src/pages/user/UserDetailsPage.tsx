import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Dashboard } from "../../components/dashboard/Dashboard";
import { getKatasFromUser } from "../../services/kataService";
// MUI
import Box from "@mui/material/Box";
import KataBox from "../../components/katas/KataBox";
import Avatar from '@mui/material/Avatar';
import { Kata } from "../../utils/types/Kata.type";
import { getUserById } from "../../services/userService";
import { User } from "../../utils/types/User.type";
import { Grid, Typography } from "@mui/material";

const UserDetailsPage = () => {

  let { userId } = useParams();

  const [user, setUser] = useState<User>({});
  const [katas, setKatas] = useState([]);

  useEffect(() => {
    getUserById(userId)
      .then((response) => {
        setUser(response.data.user);
      })
      .catch(error => console.error(error));
    getKatasFromUser(userId)
      .then((response) => {
        setKatas(response.data.katas);
      })
      .catch(error => console.error(error));
  }, []);

  return (
    <Dashboard title='User Details'>
      <Grid container sx={{my: 2}}>
        <Grid item xs={12} md={5} lg={4}>
          {
            user ?
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  my: 6,
                  mx: {
                    xs: 2,
                    md: 2,
                    lg: 6
                  }
                }}
              >
                <Avatar
                  alt={`${user.name} avatar`}
                  sx={{
                    width: '10rem',
                    height: '10rem'
                  }}
                />

                <Grid container>
                  <Grid item xs={4} sx={{mt: 2}}>
                    <Typography>
                      Name: 
                    </Typography>
                  </Grid>
                  <Grid item xs={8} sx={{mt: 2}}>
                    <Typography>
                      {user.name}
                    </Typography>
                  </Grid>

                  <Grid item xs={4} sx={{mt: 2}}>
                    <Typography>
                      Email: 
                    </Typography>
                  </Grid>
                  <Grid item xs={8} sx={{mt: 2}}>
                    <Typography>
                      {user.email}
                    </Typography>
                  </Grid>

                  <Grid item xs={4} sx={{mt: 2}}>
                    <Typography>
                      Age: 
                    </Typography>
                  </Grid>
                  <Grid item xs={8} sx={{mt: 2}}>
                    <Typography>
                      {user.age}
                    </Typography>
                  </Grid>
                </Grid>
              </Box>
              :
              null
          }
        </Grid>

        <Grid item xs={12} md={7} lg={8}>
          <Typography variant='h5'>
            Katas Created:
          </Typography>
          {
            katas ?
              katas.map((kata: Kata) => (
                <KataBox kata={kata} key={kata._id} />
              ))
              :
              null
          }
        </Grid>

      </Grid>
    </Dashboard>
  )
}

export default UserDetailsPage;