import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

//MUI
import { Box, Container, Pagination, Paper, Typography } from "@mui/material";

// Icons
import PersonIcon from '@mui/icons-material/Person';
import { getUsers } from "../../services/userService";
import { Dashboard } from "../../components/dashboard/Dashboard";

const UsersPage = () => {

  let navigate = useNavigate();

  // States
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    getUsers(currentPage, 5)
      .then((res: AxiosResponse) => {
        setUsers(res.data.users);
        setTotalPages(res.data.totalPages);
      })
      .catch(error => {
        console.error(error);
        sessionStorage.removeItem('sessionToken');
        sessionStorage.removeItem('loggedUserId');
        sessionStorage.removeItem('isAdmin');
        navigate('/');
        navigate(0);
      })
  }, [navigate, currentPage])

  /**
   * Method to navigate to User Details
   * @param id ID of kata to see details
   */
  const navigateUserDetail = (id: string) => {
    navigate(`/users/${id}`)
  }

  return (
    <Dashboard title='Users'>
      <Container maxWidth='lg'>
        <Box>
          {/* List of users */}
          {
            users ?
              <Box>
                {users.map((user: any) => (
                  <Paper
                    key={user._id}
                    sx={{
                      backgroundColor: (theme) => theme.palette.grey[100],
                      m: 2
                    }}
                  >
                    <Typography component='h2' variant='h6' onClick={() => navigateUserDetail(user._id)}>
                      <PersonIcon />
                      {user.name}
                    </Typography>

                  </Paper>
                ))}
              </Box>
              :
              <div>
                <h3>No users Found</h3>
              </div>
          }

          {/* Pagination */}
          <Pagination count={totalPages} onChange={(event, page) => setCurrentPage(page)} />
        </Box>

      </Container>
    </Dashboard>
  )

}

export default UsersPage;