import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import { getKatasFromUser } from "../../services/kataService";

// MUI
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
// Icons
import CircleIcon from '@mui/icons-material/Circle';
import PersonIcon from '@mui/icons-material/Person';
import GradeIcon from '@mui/icons-material/Grade';
import TaskIcon from '@mui/icons-material/Task';
// Types
import { Kata } from "../../utils/types/Kata.type";
import { useNavigate } from "react-router-dom";
import { Dashboard } from "../../components/dashboard/Dashboard";

const KatasFromUserPage = () => {

  // Auth
  const sessionToken = useSessionStorage('sessionToken');
  const loggedUserId = useSessionStorage('loggedUserId');

  // Katas
  const [katas, setKatas] = useState([]);
  // TODO: add pagination

  const navigate = useNavigate();

  const navigateKataDetail = (id: string) => {
    navigate(`/katas/${id}`);
  }

  useEffect(() => {
    getKatasFromUser(sessionToken)
      .then((response: AxiosResponse) => {
        if (response.status === 200) {
          setKatas(response.data.katas);
        }
      })
      .catch((error) => console.error(`[Getting Katas From User]: ${error}`))
  }, [])


  return (
    <Dashboard title='Katas from User'>
      <Box>
        {
          katas ?
            <Box>
              {katas.map((kata: Kata) => (
                <Paper
                  key={kata._id}
                  sx={{
                    backgroundColor: (theme) => theme.palette.grey[100],
                    m: 2
                  }}
                >
                  <Typography component='h2' variant='h6' onClick={() => navigateKataDetail(kata._id)}>
                    {
                      (kata.level === 'Basic' && (
                        <CircleIcon color='success' />
                      ))
                      || (kata.level === 'Medium' && (
                        <CircleIcon color='warning' />
                      ))
                      || (kata.level === 'Hard' && (
                        <CircleIcon color='error' />
                      ))
                    }
                    {kata.name}
                  </Typography>

                  <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                  >

                    <Grid item xs={4}>
                      <PersonIcon />
                      {kata.creator}
                    </Grid>
                    <Grid item xs={4}>
                      <GradeIcon />
                      {kata.stars}/5
                    </Grid>
                    <Grid item xs={4}>
                      <TaskIcon />
                      {kata.participants.length}
                    </Grid>

                  </Grid>
                </Paper>
              ))}
            </Box>
            :
            <div>
              <h3>No Katas Found</h3>
            </div>
        }
      </Box>
    </Dashboard>
  )
}

export default KatasFromUserPage;