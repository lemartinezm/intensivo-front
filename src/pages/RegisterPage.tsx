import RegisterForm from "../components/forms/RegisterForm";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import logo from '../components/forms/openb.png';
import { StickyFooter } from "../components/dashboard/StickyFooter";
import { Link } from 'react-router-dom';

const RegisterPage = () => {

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Box>
        <img style={{ width: '5rem' }} src={logo} alt="Logo OpenBootcamp" />
        <Typography component='h2' variant='h5'>
          Register
        </Typography>
      </Box>

      <RegisterForm />
      <Link to='./login' >{"Already have an account? Sign in"}</Link>
      <StickyFooter />
    </Box>
  )

}

export default RegisterPage;