import { Dashboard } from "../../components/dashboard/Dashboard";
import CreateKataForm from "../../components/forms/CreateKataForm";
import { useSessionStorage } from "../../hooks/useSessionStorage";

const CreateKataPage = (props: any) => {

  let sessionToken = useSessionStorage('sessionToken');

  return (
    <Dashboard title='Create Kata'>
      <CreateKataForm token={sessionToken}></CreateKataForm>
    </Dashboard>
  )
};

export default CreateKataPage;