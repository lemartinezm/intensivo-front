import { Box, Button, Container, MenuItem, Rating, TextField, Typography } from "@mui/material";
import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import { getKataById, solveKata, voteKata } from "../../services/kataService";
import { Kata } from "../../utils/types/Kata.type";
// Icons
import PersonIcon from '@mui/icons-material/Person';
// Editor
import CodeEditor from "@uiw/react-textarea-code-editor";
// Consts
import { languagesAvailable } from '../../components/consts/languagesAvailable';
import { Dashboard } from "../../components/dashboard/Dashboard";
import CircleColorLevel from "../../components/katas/CircleColorLevel";
import { verifyMe } from "../../services/authService";

const KatasDetailPage = (props: any) => {

  let loggedIn = useSessionStorage('sessionToken');
  const loggedUserId = useSessionStorage('loggedUserId');
  const gridConfigSx: any = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  };

  let navigate = useNavigate();

  // Kata Id from params
  let { id } = useParams();

  // States
  const [kata, setKata] = useState<Kata | undefined>(undefined);
  const [showSolution, setShowSolution] = useState(false);
  const [showEditButton, setShowEditButton] = useState(false);
  const [kataTried, setKataTried] = useState(false);
  const [showSolveBox, setShowSolveBox] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState('js');
  const [code, setCode] = useState('');

  useEffect(() => {
    verifyMe()
      .then((res) => {
        if (res.status === 200 && res.data.isAdmin === true) {
          setShowEditButton(true);
        }
      })
      .catch(error => console.error(error))

    if (id) {
      getKataById(loggedIn, id)
        .then((response: AxiosResponse) => {
          if (response.status === 200 && response.data.kata) {
            setKata(response.data.kata);
            if (response.data.kata.creator.creatorId === loggedUserId) {
              setShowEditButton(true);
            }
            if (response.data.kata.solution) {
              setKataTried(true);
            }
          }
        })
        .catch((error) => console.error(`[Get Kata by Id Error]: ${error}`));
    }
  }, [kataTried]);

  return (
    <Dashboard title='Kata Details'>
      <Container maxWidth='lg'>
        <Box>

          {
            kata ?
              <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}>

                <Box>
                  <Typography component='h1' variant='h4' sx={gridConfigSx}>
                    <CircleColorLevel kata={kata} />
                    {kata.name}
                  </Typography>

                  <Typography sx={gridConfigSx}>
                    <PersonIcon /> {kata.creator.creatorName}
                  </Typography>
                </Box>

                <Box sx={{ mt: 2 }}>
                  <Typography variant='h5'>
                    Description:
                  </Typography>
                  <Typography sx={{
                    whiteSpace: 'pre-wrap',
                    textAlign: 'justify'
                  }}>
                    {kata.description}
                  </Typography>
                </Box>


                {/* Try kata section */}

                <Button
                  variant='contained'
                  sx={{ m: 2 }}
                  onClick={() => setShowSolveBox(!showSolveBox)}
                >
                  {showSolveBox ? 'Hide' : 'Solve kata'}
                </Button>

                {
                  showSolveBox ?
                    <Box sx={{ m: 2 }} maxWidth='md'>
                      <TextField
                        select
                        required
                        margin="normal"
                        helperText="Select language"
                        value={selectedLanguage}
                        onChange={(event) => setSelectedLanguage(event.target.value)}
                      >
                        {languagesAvailable.map((language) => (
                          <MenuItem key={language.value} value={language.value}>
                            {language.label}
                          </MenuItem>
                        ))}
                      </TextField>

                      <CodeEditor
                        language={selectedLanguage}
                        placeholder='Write kata solution here'
                        value={code}
                        onChange={(evn) => setCode(evn.target.value)}
                        id='solution'
                        name='solution'
                        padding={15}
                        style={{
                          fontSize: 12,
                          backgroundColor: "#f5f5f5",
                          fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                          width: '50rem',
                          height: '10rem',
                          marginBottom: '2rem'
                        }}
                      />

                      <Button
                        variant="contained"
                        onClick={() => {
                          solveKata(loggedIn, id!, code)
                            .then((response) => {
                              if (response.status === 200) {
                                alert('Solution sent successfully');
                                setKataTried(true);
                              }
                            })
                            .catch((error) => console.error(error))
                        }}>
                        Send Solution
                      </Button>
                    </Box>
                    :
                    null
                }

                {/* Show solution if kata was tried before */}
                {
                  kataTried ?
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <Box>
                        <Typography>
                          Rate this Kata:
                        </Typography>
                        <Rating
                          onChange={(evn, value) => voteKata(kata._id, value).then((res) => console.log('###', res))}
                        />
                      </Box>

                      <Button
                        sx={{
                          mt: 2
                        }}
                        variant="contained"
                        onClick={() => setShowSolution(!showSolution)}
                      >
                        {showSolution ? 'Hide Solution' : 'Show Solution'}
                      </Button>
                    </Box>
                    :
                    null
                }

                {
                  showSolution ?
                    <Box sx={{
                      textAlign: 'justify',
                      my: 2
                    }}>
                      {/* <Editor language='typescript'>{kata.solution}</Editor> */}
                      <CodeEditor
                        language={kata.language}
                        value={kata.solution}
                        id='solutionExample'
                        name='solutionExample'
                        padding={15}
                        style={{
                          fontSize: 12,
                          backgroundColor: "#f5f5f5",
                          fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                          width: '50rem',
                          height: '10rem'
                        }}
                      />
                    </Box>
                    :
                    null
                }

                {/* Edit the kata if you are the creator or admin */}
                {
                  showEditButton ?
                    <Button
                      variant='contained'
                      sx={{
                        mt: 2
                      }}
                      onClick={() => navigate('./edit')}
                    >
                      Edit Kata
                    </Button>
                    :
                    null
                }

              </Box>
              :
              <div>
                <h3>Kata Not Found</h3>
              </div>
          }
        </Box>

      </Container>
    </Dashboard>
  )

}

export default KatasDetailPage;