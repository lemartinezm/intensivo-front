import { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import { getAllKatas } from "../../services/kataService";
import { Kata } from "../../utils/types/Kata.type";

//MUI
import { Box, Button, Container, Grid, MenuItem, Pagination, TextField } from "@mui/material";

// Icons

import CircularProgress from '@mui/material/CircularProgress';

// Consts
import { languagesAvailable } from "../../components/consts/languagesAvailable";
import { levelValues } from "../../components/consts/levelValues";
import { sortValues } from "../../components/consts/sortValues";
import { Dashboard } from "../../components/dashboard/Dashboard";
import KataBox from "../../components/katas/KataBox";

const KatasPage = (props: any) => {

  let loggedIn = useSessionStorage('sessionToken');
  let navigate = useNavigate();

  // States
  const [katas, setKatas] = useState([]);
  const [sort, setSort] = useState('');
  const [levelFilter, setLevelFilter] = useState('');
  const [language, setLanguage] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [limitKatas, setLimitKatas] = useState(10);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getAllKatas(loggedIn, currentPage, limitKatas, levelFilter, language, sort)
      .then((response: AxiosResponse) => {
        if (response.status === 200 && response.data) {
          setKatas(response.data.katas);
          setCurrentPage(response.data.currentPage);
          setTotalPages(response.data.totalPages);
        } else {
          throw new Error('Error getting all katas');
        }
        setLoading(false);
      })
      .catch((error) => {
        console.error(`[Get All Katas Error]: ${error}`);
        sessionStorage.removeItem('sessionToken');
        sessionStorage.removeItem('loggedUserId');
        sessionStorage.removeItem('isAdmin');
        navigate('/');
        navigate(0);
      });

  }, [loggedIn, navigate, currentPage, sort, levelFilter, limitKatas, language])

  const handleReset = () => {
    setSort('');
    setLanguage('');
    setLevelFilter('');
    setLimitKatas(10);
  }


  return (
    <Dashboard title='Katas'>
      <Container maxWidth='xl'>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'flex-end'
          }}
        >
          <Box
            sx={{
              px: 2
            }}
          >
            <Button variant='contained' onClick={() => navigate('./create')} >Create Kata</Button>
          </Box>

          <Grid container>
            <Grid item xs={12} md={4}
              sx={{
                my: 2,
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  width: '100%',
                  borderRadius: 2,
                  backgroundColor: (theme) => theme.palette.grey[50]
                }}
              >
                <Box
                  sx={{
                    width: '75%',
                    mt: 2
                  }}
                >
                  <TextField
                    select
                    fullWidth
                    margin='normal'
                    id='sort'
                    name='sort'
                    label='Sort by'
                    value={sort}
                    onChange={(evn) => setSort(evn.target.value)}
                  >
                    {
                      sortValues.map(sort => (
                        <MenuItem value={sort.value} key={sort.value}>
                          {sort.label}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Box>

                <Box
                  sx={{
                    width: '75%',
                    mt: 2
                  }}
                >
                  <TextField
                    select
                    fullWidth
                    margin="normal"
                    id='language'
                    name='language'
                    label='Language'
                    value={language}
                    onChange={(evn) => setLanguage(evn.target.value)}
                  >
                    {languagesAvailable.map((language) => (
                      <MenuItem key={language.value} value={language.value}>
                        {language.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Box>

                <Box
                  sx={{
                    width: '75%',
                    mt: 2
                  }}
                >
                  <TextField
                    select
                    fullWidth
                    margin='normal'
                    id='level'
                    name='level'
                    label='Level filter'
                    value={levelFilter}
                    onChange={(evn) => setLevelFilter(evn.target.value)}
                  >
                    {
                      levelValues.map((level) => (
                        <MenuItem key={level.value} value={level.value}>
                          {level.label}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Box>

                <Box
                  sx={{
                    width: '75%',
                    mt: 2
                  }}
                >
                  <TextField
                    type='number'
                    margin='normal'
                    id='limit'
                    name='limit'
                    placeholder="10"
                    label='Limit'
                    value={limitKatas}
                    onChange={(evn) => setLimitKatas(parseInt(evn.target.value))}
                  />
                </Box>

                <Box
                  sx={{
                    width: '75%',
                    mt: 2,
                    mb: 2
                  }}
                >
                  <Button variant='contained' onClick={handleReset} >Reset</Button>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={8}>
              <Box>
                {
                  loading ?
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100%',
                        height: '100%'
                      }}
                    >
                      <CircularProgress variant="indeterminate" disableShrink />
                    </Box>
                    :
                    katas ?
                      <Box>
                        {katas.map((kata: Kata) => (
                          <KataBox kata={kata} key={kata._id} />
                        ))}
                      </Box>
                      :
                      <div>
                        <h3>No Katas Found</h3>
                      </div>

                }
              </Box>
              {/* Pagination */}
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center'
                }}
              >
                <Pagination count={totalPages} onChange={(event, page) => setCurrentPage(page)} />
              </Box>
            </Grid>
          </Grid>


        </Box>

      </Container>
    </Dashboard>
  )

}

export default KatasPage;