import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import { AxiosResponse } from "axios";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Dashboard } from "../../components/dashboard/Dashboard";
import EditKatasForm from "../../components/forms/EditKatasForm";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import { deleteKata } from "../../services/kataService";

const EditKatasPage = () => {

  const loggedIn = useSessionStorage('sessionToken');
  const { id } = useParams();
  const navigate = useNavigate();

  // States
  const [confirmDelete, setConfirmDelete] = useState(false);


  const handleDelete = () => {
    deleteKata(loggedIn, id!)
      .then((response: AxiosResponse) => {
        if (response.status === 200) {
          alert('Kata deleted successfully');
          navigate(-1);
        } else {
          throw new Error('Error deleting Kata');
        }
      })
      .catch((error) => {
        console.error(`[Deleting Kata]: ${error}`);
        alert('You are not authorized to delete this Kata');
      })
  }


  return (
    <Dashboard title='Edit Kata'>
      <EditKatasForm loggedIn={loggedIn} id={id} />

      <Box>
        {/* Delete Section */}
        <Button variant='contained' color='error' onClick={() => setConfirmDelete(true)}>Delete Kata</Button>

        <Dialog
          open={confirmDelete}
          onClose={() => setConfirmDelete(false)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {'Delete Confimation'}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {'Are you sure? This action can\'t be undone.'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" color="error" onClick={() => handleDelete()} >
              Yes, delete Kata
            </Button>

            <Button variant='contained' onClick={() => setConfirmDelete(false)}>
              No, preserve Kata
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    </Dashboard>
  )
}

export default EditKatasPage;