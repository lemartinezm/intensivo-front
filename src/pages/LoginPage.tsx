import Box from "@mui/material/Box";
import LoginForm from "../components/forms/LoginForm";
import Typography from "@mui/material/Typography";
import logo from '../components/forms/openb.png';
import { StickyFooter } from "../components/dashboard/StickyFooter";
import { Link } from 'react-router-dom';

const LoginPage = () => {

  return (
    <Box
      sx={{
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Box>
        <img style={{ width: '5rem' }} src={logo} alt="Logo OpenBootcamp" />
        <Typography component='h2' variant='h5'>
          Login
        </Typography>
      </Box>

      <LoginForm />
      <Link to='./register' >{"Don't have an account? Sign up"}</Link>
      <StickyFooter />
    </Box>

  )

}

export default LoginPage;