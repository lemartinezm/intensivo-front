import { Box } from "@mui/material";
import { Navigate, useNavigate } from "react-router-dom";
import { Dashboard } from "../../components/dashboard/Dashboard";
import Button from "@mui/material/Button";

const AdminPage = (props: any) => {

  const navigate = useNavigate();

  return (
    <Dashboard title='Admin Page'>
      {
        props.isAdmin ?
        <Box>
          <Button variant='contained' onClick={() => navigate('./register')} >Register New User</Button>
        </Box>
        :
        <Navigate to='/' replace></Navigate>
      }
    </Dashboard>
  )
}

export default AdminPage;