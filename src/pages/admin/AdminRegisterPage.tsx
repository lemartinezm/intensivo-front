import { Dashboard } from "../../components/dashboard/Dashboard";
import RegisterForm from "../../components/forms/RegisterForm";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";

const AdminRegisterPage = () => {

  return (
    <Dashboard>
      <Box>
        <Typography component='h2' variant='h5'>
          Register User
        </Typography>
      </Box>
      <RegisterForm />
    </Dashboard>
  )
}

export default AdminRegisterPage;