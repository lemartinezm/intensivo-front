import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import { Dashboard } from "../components/dashboard/Dashboard";
import { AppRoutes } from "../routes/Routes";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";

const TestPage = (props: any) => {
  if (props.validToken) {
    return (
      <Router>
        <Dashboard isAdmin={false}>
          <AppRoutes isAdmin={false} />
        </Dashboard>
      </Router>

    )
  } else {
    return (
      <Router>
        <Routes>
          <Route path='/register' element={<RegisterPage />} ></Route>
          <Route path='/*' element={<LoginPage />} ></Route>
        </Routes>
      </Router>

    )
  }
}

export default TestPage;