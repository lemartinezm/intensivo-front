export type Kata = {
  _id: string,
  name: string,
  description: string,
  level: string,
  intents: number,
  stars: number,
  creator: any,
  language: string,
  solution: string,
  participants: []
}

export type CreateKata = {
  name: string,
  description: string,
  level: string,
  solution: string
}

export type UpdateKata = {
  name?: string,
  description?: string,
  level?: string,
  language?: string,
  solution?: string
}