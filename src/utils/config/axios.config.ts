import axios from 'axios';

export default axios.create({
  baseURL: 'https://intensivo-back.onrender.com/api',
  // baseURL: 'http://localhost:8000/api',
  responseType: 'json',
  timeout: 10000
})