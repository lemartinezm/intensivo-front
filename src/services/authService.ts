import axios from '../utils/config/axios.config';

/**
 * Login Method
 * @param {string} email User's email
 * @param {string} password User's password
 * @returns JSON
 */
export const login = (email: string, password: string) => {
  // Body to send
  const body: any = {
    email,
    password
  };

  // Send body to URL
  return axios.post('/auth/login', body);
}

/**
 * Register Method
 * @param {string} name User's name
 * @param {string} email User's email
 * @param {string} password User's password
 * @param {number} age User's age
 * @returns JSON
 */
export const register = (name: string, email: string, password: string, age: number, role?: string, isAdmin?: boolean) => {
  // Body to send
  const body: any = {
    name,
    email,
    password,
    age,
    role
  };

  if (isAdmin) {
    return axios.post('/auth/register/admin', body)
  } else {
    return axios.post('/auth/register', body);
  }
}

/**
 * Method to validate the Token stored in SessionStorage
 * @returns AxiosResponse with validation or rejection of the Token
 */
export const verifyMe = () => {
  const sessionToken: any = sessionStorage.getItem('sessionToken');
  return axios.get('/auth/me', {
    headers: {
      'x-access-token': sessionToken
    }
  })
}