import { AxiosResponse } from "axios";
import axios from "../utils/config/axios.config";

export const getUsers = (page?: number, limit?: number) => {
  const sessionToken: any = sessionStorage.getItem('sessionToken');
  return axios.get('/users', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      page,
      limit
    }
  })
}

export const getUserById = (userId: string | undefined) => {
  const sessionToken: any = sessionStorage.getItem('sessionToken');
  return axios.get('/users', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: userId
    }
  })
}