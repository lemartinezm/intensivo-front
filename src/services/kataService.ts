import { AxiosResponse } from "axios";
import axios from "../utils/config/axios.config";
import { CreateKata, UpdateKata } from "../utils/types/Kata.type";

/**
 * Método para obtener todas las katas
 * @param {string} sessionToken Token del usuario obtenido de SessionStorage
 * @returns {AxiosResponse} AxiosResponse (object) que contiene el JSON enviado por la API dentro del campo "data".
 */
export const getAllKatas = (sessionToken: string,
  page?: number,
  limit?: number,
  level?: string,
  language?: string,
  sortType?: string): Promise <AxiosResponse> => {
  return axios.get('/katas', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      page,
      limit,
      level,
      sortType,
      language
    }
  });
}

/**
 * Method to get Kata by ID
 * @param {string} sessionToken User Token
 * @param {string} kataId Kata ID to request
 * @returns {AxiosResponse} AxiosResponse (object) that contains data requested in the field "data".
 */
export const getKataById = (sessionToken: string, kataId: string): Promise <AxiosResponse> => {
  return axios.get('/katas', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: kataId
    }
  });
}

/**
 * Método para obtener las Katas creadas por un Usuario mediante su ID.
 * @param {string} userId ID del usuario del que se quiere obtener sus Katas. Si no se enviara un ID, se recupera las katas del usuario loggeado.
 * @returns {AxiosResponse} AxiosResponse (object) que contiene el JSON enviado por la API dentro del campo "data".
 */
export const getKatasFromUser = (userId: string | undefined): Promise <AxiosResponse> => {
  const sessionToken: any = sessionStorage.getItem('sessionToken');
  return axios.get('/users/katas', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: userId
    }
  })
}

/**
 * Método para crear una Kata.
 * @param {string} sessionToken Token del usuario obtenido de SessionStorage
 * @param {CreateKata} kata Object que contiene la información de la Kata que será creada.
 * @returns {AxiosResponse} AxiosResponse (object) que contiene el JSON enviado por la API dentro del campo "data".
 */
export const createKata = (sessionToken: string, kata: CreateKata): Promise <AxiosResponse> => {
  return axios.post('/katas', kata, {
    headers: {
      'x-access-token': sessionToken
    }
  })
}

/**
 * Método para actualizar una Kata por ID
 * @param {string} sessionToken Token del usuario obtenido de SessionStorage
 * @param {string} kataId ID de la kata que se desea actualizar
 * @param {UpdateKata} kata Object que contiene la información actualizada de la Kata.
 * @returns {AxiosResponse} AxiosResponse (object) que contiene el JSON enviado por la API dentro del campo "data".
 */
export const updateKata = (sessionToken: string, kataId: string, kata: UpdateKata): Promise <AxiosResponse> => {
  return axios.put('/katas', kata, {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: kataId
    }
  })
}

/**
 * Método para eliminar una kata por ID
 * @param {string} sessionToken Token del usuario obtenido de SessionStorage
 * @param {string} kataId ID de la kata que se desea eliminar
 * @returns {AxiosResponse} AxiosResponse (object) que contiene el JSON enviado por la API dentro del campo "data".
 */
export const deleteKata = (sessionToken: string, kataId: string): Promise <AxiosResponse> => {
  return axios.delete('/katas', {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: kataId
    }
  })
}

/**
 * Method to solve a Kata
 * @param {string} sessionToken Logged user token
 * @param {string} kataId ID of the kata that is trying to solve
 * @param {string} solution The solution of the kata
 * @returns {AxiosResponse} AxiosResponse that have the object sent by the API in the field "data".
 */
export const solveKata = (sessionToken: string, kataId: string, solution: string): Promise <AxiosResponse> => {
  return axios.put('/katas/solve', {
    solution: solution
  }, {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: kataId
    }
  })
}

export const voteKata = (kataId: string, valoration: any) => {
  const sessionToken: any = sessionStorage.getItem('sessionToken');
  return axios.put('/katas/vote', {
    valoration
  }, {
    headers: {
      'x-access-token': sessionToken
    },
    params: {
      id: kataId
    }
  })
}