import { BrowserRouter as Router} from 'react-router-dom';
import './App.css';
import { AppRoutes, NotLoggedRoutes } from './routes/Routes';
import { useEffect, useState } from 'react';
import { verifyMe } from './services/authService';


function App() {

  const [validToken, setValidToken] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    verifyMe()
      .then((res) => {
        if (res.status === 200) {
          setValidToken(true);
          if (res.data.isAdmin === true) {
            setIsAdmin(true)
          }
        }
      })
      .catch(error => console.error(error))
  }, [])

  return (
    <div className="App">

      {/* Router for navigation between pages */}
      <Router>
        {
          validToken ?
            <AppRoutes isAdmin={isAdmin} />
            :
            <NotLoggedRoutes />
        }
      </Router>
    </div>
  );
}

export default App;
