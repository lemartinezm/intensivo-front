import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
// MUI
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import MuiDrawer from "@mui/material/Drawer";
import { createTheme, styled, ThemeProvider } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Container from "@mui/material/Container";
import Badge from "@mui/material/Badge";
// MUI List
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

// Icons
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import LogoutIcon from '@mui/icons-material/Logout';
import NotificationsIcon from '@mui/icons-material/Notifications'
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';

import { MenuItems } from "./MenuItems";
import { verifyMe } from "../../services/authService";
import { StickyFooter } from "./StickyFooter";

const drawerWidth: number = 240;

interface AppBarProps extends MuiAppBarProps {
  open?: boolean
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open'
})<AppBarProps>(({ theme, open }) => (
  {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    ...(open && {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    })
  }
));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open'
})(({ theme, open }) => ({
  '& .MuiDrawer-paper': {
    position: 'relative',
    width: drawerWidth,
    whiteSpace: 'nowrap',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    boxSizing: 'border-box',
    ...(!open && {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      }),
      width: theme.spacing(7),
      // Breakpoints to Media Queries
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9)
      }
    })
  }
}))

// Define Theme
const myTheme = createTheme({
  palette: {
    primary: {
      main: '#2f4384'
    }
  }
});

export const Dashboard = (props?: any) => {

  const pageTitle = props.title;
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    verifyMe()
      .then((res) => {
        if (res.status === 200 && res.data.isAdmin === true) {
          setIsAdmin(true)
        }
      })
      .catch(error => console.error(error))
  }, [])

  const logout = () => {
    sessionStorage.removeItem('sessionToken');
    sessionStorage.removeItem('loggedUserId');
    sessionStorage.removeItem('isAdmin');
    navigate('/');
    navigate(0);
  }

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />

        <AppBar position='absolute' open={open}>
          <Toolbar sx={{ pr: '24px' }}>
            <IconButton
              edge='start'
              color='inherit'
              aria-label="open drawer"
              onClick={() => setOpen(!open)}
              sx={{
                marginRight: '36px',
                ...(open && {
                  display: 'none'
                })
              }}
            >
              <MenuIcon />
            </IconButton>

            <Typography
              component='h1'
              variant='h6'
              color='inherit'
              noWrap
              sx={{ flexGrow: 1 }}
            >
              {pageTitle}
            </Typography>

            {/* Notification Icon */}
            <IconButton>
              <Badge badgeContent={10} color='secondary'>
                <NotificationsIcon />
              </Badge>
            </IconButton>

            {/* Logout Icon */}
            <IconButton onClick={logout} component={Link} to='/'>
              <LogoutIcon />
            </IconButton>
          </Toolbar>
        </AppBar>

        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1]
            }}
          >

            <IconButton color='inherit' onClick={() => setOpen(!open)}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>

          <Divider />
          <List component='nav'>
            {MenuItems}

            {
              isAdmin ?
                < ListItemButton component={Link} to='/admin' >
                  <ListItemIcon>
                    <AdminPanelSettingsIcon />
                  </ListItemIcon>
                  <ListItemText primary='Admin' />
                </ListItemButton >
                :
                null
            }

          </List>
        </Drawer>

        <Box
          component='main'
          sx={{
            backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto'
          }}
        >
          <Toolbar />
          <Container
            maxWidth='xl'
          >

            <Box
              sx={{
                py: 2,
                display: 'flex',
                flexDirection: 'column',
                height: 'auto'
              }}
            >

              {props.children}

            </Box>

            <StickyFooter />

          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}