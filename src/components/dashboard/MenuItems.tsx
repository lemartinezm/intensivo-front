import React from 'react';
import { Link } from "react-router-dom";

// MUI
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from '@mui/material/ListItemIcon';

// Icons
import DashboardIcon from "@mui/icons-material/Dashboard";
import PeopleIcon from "@mui/icons-material/People";
import BarChart from '@mui/icons-material/BarChart';
import HomeIcon from '@mui/icons-material/Home';

export const MenuItems = (
  < React.Fragment >
    {/* Home */}
    < ListItemButton component={Link} to='/'>
      <ListItemIcon>
        <HomeIcon />
      </ListItemIcon>
      <ListItemText primary='Home' />
    </ListItemButton >

    {/* Katas Button */}
    < ListItemButton component={Link} to='/katas'>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary='Katas' />
    </ListItemButton >

    {/* Users Button */}
    < ListItemButton component={Link} to='/users' >
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary='Users' />
    </ListItemButton >

    {/* Ranking Button */}
    < ListItemButton component={Link} to='/ranking'>
      <ListItemIcon>
        <BarChart />
      </ListItemIcon>
      <ListItemText primary='Ranking' />
    </ListItemButton >
  </React.Fragment >
)

