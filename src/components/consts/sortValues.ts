export const sortValues = [
  {
    value: 'level_asc',
    label: 'Level Ascendent'
  },
  {
    value: 'level_des',
    label: 'Level Descendent'
  },
  {
    value: 'intents_asc',
    label: 'Intents Ascendent'
  },
  {
    value: 'intents_des',
    label: 'Intents Descendent'
  },
  {
    value: 'name_asc',
    label: 'Name Ascendent'
  },
  {
    value: 'name_des',
    label: 'Name Descendent'
  },
  {
    value: 'stars_asc',
    label: 'Stars Ascendent'
  },
  {
    value: 'stars_des',
    label: 'Stars Descendent'
  }
]
