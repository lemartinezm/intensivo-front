export interface ILanguageSVG {
  ino: any,
  c: any,
  cpp: any,
  cs: any,
  css: any,
  go: any,
  java: any,
  js: any,
  kotlin: any,
  md: any,
  php: any,
  py: any,
  ruby: any,
  sass: any,
  sql: any,
  ts: any
}