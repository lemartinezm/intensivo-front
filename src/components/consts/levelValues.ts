export const levelValues = [
  {
    value: 'Basic',
    label: 'Basic'
  },
  {
    value: 'Medium',
    label: 'Medium'
  },
  {
    value: 'Hard',
    label: 'Hard'
  }
]