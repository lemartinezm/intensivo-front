// MUI Material
import Paper from "@mui/material/Paper";
import { Grid, Typography } from "@mui/material";
import Rating from '@mui/material/Rating';
import CircleColorLevel from "./CircleColorLevel";
// Const
import { languageSVG } from "../consts/languagesAvailable";
import { ILanguageSVG } from "../consts/types/language.type";
// Icons
import PersonIcon from '@mui/icons-material/Person';
import TaskIcon from '@mui/icons-material/Task';
import { useNavigate } from "react-router-dom";

const KataBox = (props: any) => {

  const gridConfigSx: any = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  };

  let navigate = useNavigate();

  const kata = props.kata;

  /**
   * Method to navigate to Kata Details
   * @param id ID of kata to see details
   */
  const navigateKataDetail = (id: string) => {
    navigate(`/katas/${id}`);
  }

  const navigateUserDetail = (id: string) => {
    navigate(`/users/${id}`);
  }

  return (
    <Paper
      key={kata._id}
      sx={{
        backgroundColor: (theme) => theme.palette.grey[50],
        m: 2,
        py: 1,
      }}
    >
      <Typography
        component='h2'
        variant='h6'
        onClick={() => navigateKataDetail(kata._id)}
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <CircleColorLevel kata={kata} />
        {kata.name}
      </Typography>

      <Grid
        container
        alignItems="center"
        sx={{
          px: 3
        }}
      >

        <Grid item xs={12} sm={4} sx={gridConfigSx}>
          <PersonIcon />
          <Typography
            onClick={() => navigateUserDetail(kata.creator.creatorId)}
          >
            {kata.creator.creatorName}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={4} sx={gridConfigSx}>
          <Rating
            name="rating"
            value={kata.stars}
            precision={0.1}
            readOnly
          />
        </Grid>

        <Grid item xs={12} sm={3} sx={gridConfigSx}>
          <TaskIcon />
          {kata.participants.length}
        </Grid>

        <Grid item xs={12} sm={1} sx={gridConfigSx}>
          {languageSVG[kata.language as keyof ILanguageSVG]}
        </Grid>
      </Grid>
    </Paper>
  )
}

export default KataBox;