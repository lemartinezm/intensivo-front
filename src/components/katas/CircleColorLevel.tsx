import React from 'react';
import CircleIcon from '@mui/icons-material/Circle';

const CircleColorLevel = (props: any) => {
  const kata: any = props.kata
  return (
    <React.Fragment>
      {
      (kata.level === 'Basic' && (
        <CircleIcon color='success' sx={{mr: 1}}/>
      ))
      || (kata.level === 'Medium' && (
        <CircleIcon color='warning' sx={{mr: 1}}/>
      ))
      || (kata.level === 'Hard' && (
        <CircleIcon color='error' sx={{mr: 1}}/>
      ))
    }
    </React.Fragment>
  )
}

export default CircleColorLevel;