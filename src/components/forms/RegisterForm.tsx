import { AxiosResponse } from 'axios';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { register, verifyMe } from '../../services/authService';
// MUI imports
import { Container, CssBaseline, MenuItem, TextField } from '@mui/material';
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
// For admin register
import { useEffect, useState } from 'react';

const registerSchema = Yup.object().shape({
  name: Yup.string()
    .min(4, 'Name must have 4 letters minimum')
    .max(16, 'Name must have 16 letters maximum')
    .required('Name is required'),
  email: Yup.string()
    .email('Invalid email format')
    .required('Email is required'),
  password: Yup.string()
    .min(8, 'Password is too short')
    .required('Password is required'),
  confirm: Yup.string()
    .when('password', {
      is: (value: string) => (value && value.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref('password')], 'Passwords must match'
      )
    })
    .required('You must confirm your password'),
  age: Yup.number()
    .min(15, 'You must be over 15 years old')
    .required('Age is required'),
  role: Yup.string()
})

// * Register Component
const RegisterForm = () => {

  const navigate = useNavigate();
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    verifyMe()
      .then((res: AxiosResponse) => {
        if (res.status === 200) {
          if (res.data.isAdmin === true) {
            setIsAdmin(true);
          } else {
            navigate('/');
          }
        }
      })
      .catch(error => {
        console.error(error);
      })
  }, [])

  // Initial credentials
  const initialCredentials = {
    name: '',
    email: '',
    password: '',
    confirm: '',
    age: 15,
    role: 'user'
  }

  // Formik Config to use with MUI Components
  const formik = useFormik({
    initialValues: initialCredentials,
    validationSchema: registerSchema,
    onSubmit: async (values) => {
      if (isAdmin) {
        register(values.name, values.email, values.password, values.age, values.role, true).then((response: AxiosResponse) => {
          if (response.status === 201) {
            alert('User registered successfully');
            navigate('/');
          } else {
            alert('User could not be registered')
            throw new Error('Error registering user')
          }
        }).catch((error) => {
          if (error.response.data) {
            console.error(`[REGISTER ERROR]: Something went wrong: ${error.response.data.message}`);
            alert(error.response.data.message);
          } else {
            console.error(error)
            alert('Something went wrong: Registration failed.');
          }
        })
      } else {
        register(values.name, values.email, values.password, values.age).then((response: AxiosResponse) => {
          if (response.status === 201) {
            alert('User registered successfully');
            navigate('/');
          } else {
            alert('User could not be registered')
            throw new Error('Error registering user')
          }
        }).catch((error) => {
          if (error.response.data) {
            console.error(`[REGISTER ERROR]: Something went wrong: ${error.response.data.message}`);
            alert(error.response.data.message);
          } else {
            console.error(error)
            alert('Something went wrong: Registration failed.');
          }
        })
      }
    }
  })

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />

      <Box
        sx={{ mt: 1 }}
        component="form"
        onSubmit={formik.handleSubmit}
      >
        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='name'
          type='text'
          name='name'
          placeholder='Your Name'
          label='Name'
          value={formik.values.name}
          onChange={formik.handleChange}
          error={formik.touched.name && Boolean(formik.errors.name)}
          helperText={formik.touched.name && formik.errors.name}
        />

        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='email'
          type='email'
          name='email'
          placeholder='example@email.com'
          label='Email'
          autoComplete='email'
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
        />

        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='password'
          type='password'
          name='password'
          placeholder='***********'
          label='Password'
          autoComplete='new-password'
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />

        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='confirm'
          type='password'
          name='confirm'
          placeholder='*********'
          label='Confirm Password'
          autoComplete='new-password'
          value={formik.values.confirm}
          onChange={formik.handleChange}
          error={formik.touched.confirm && Boolean(formik.errors.confirm)}
          helperText={formik.touched.confirm && formik.errors.confirm}
        />

        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='age'
          type='number'
          name='age'
          placeholder='15'
          label='Age'
          value={formik.values.age}
          onChange={formik.handleChange}
          error={formik.touched.age && Boolean(formik.errors.age)}
          helperText={formik.touched.age && formik.errors.age}
        />

        {
          isAdmin ?
            <TextField
              required
              select
              fullWidth
              margin='normal'
              id='role'
              name='role'
              label='role'
              value={formik.values.role}
              onChange={formik.handleChange}
              helperText='Select role'
            >
              <MenuItem value={'user'}>
                User
              </MenuItem>
              <MenuItem value={'admin'}>
                Admin
              </MenuItem>
            </TextField>
            :
            null
        }

        <Button
          fullWidth
          variant='contained'
          type="submit"
          sx={{
            my: 2
          }}
        >
          {
            isAdmin ?
              'Register'
              :
              'Sign Up'
          }
        </Button>

      </Box>
    </Container>

  )
}

export default RegisterForm;