import { login } from "../../services/authService";
import * as Yup from 'yup'
import { useFormik } from "formik";
import { AxiosResponse } from "axios";
import { useNavigate } from "react-router-dom";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import { Container } from "@mui/material";

// Define Schema Validation with Yup
const loginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email format').required('Email is required'),
  password: Yup.string().min(8, 'Password too short').required('Password is required')
})

// Login Component
const LoginForm = () => {

  // Define initial credentials
  const initialCredentials = {
    email: '',
    password: ''
  }

  const navigate = useNavigate();

  // Formik Config to use with MUI Components
  const formik = useFormik({
    initialValues: initialCredentials,
    validationSchema: loginSchema,
    onSubmit: async (values) => {
      login(values.email, values.password).then((response: AxiosResponse) => {
        if (response.status === 200) {
          if (response.data.token) {
            localStorage.setItem('loginUser', JSON.stringify(response.data.user));
            sessionStorage.setItem('sessionToken', response.data.token);
            sessionStorage.setItem('loggedUserId', response.data.loggedUserId);
            sessionStorage.setItem('isAdmin', response.data.admin);
            navigate(0);
          } else {
            throw new Error('Error generating Login Token')
          }
        } else {
          throw new Error('Invalid credentials')
        }
      }).catch((error) => console.error(`[LOGIN ERROR]: Something went wrong: ${error}`))
    }
  })

  return (
    <Container component='main' maxWidth='xs'
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>

      <Box
        component='form'
        onSubmit={formik.handleSubmit}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%'
        }}
      >
        <TextField
          required
          fullWidth
          margin="normal"
          variant="outlined"
          id='email'
          type='email'
          name='email'
          placeholder='example@email.com'
          label='Email'
          autoComplete="email"
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
        />

        <TextField
          required
          fullWidth
          margin='normal'
          variant="outlined"
          id='password'
          type='password'
          name='password'
          placeholder='********'
          label='Password'
          autoComplete="current-password"
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />
        <Button
          fullWidth
          variant='contained'
          type="submit"
          sx={{
            my: 3
          }}
        >
          Sign In
        </Button>

      </Box>

    </Container>
  )
}

export default LoginForm;