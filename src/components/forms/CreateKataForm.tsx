import { AxiosResponse } from 'axios';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { createKata } from '../../services/kataService';

// MUI imports
import Button from '@mui/material/Button';
import { Box, Container, CssBaseline, TextField, Typography } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
// Editor
import CodeEditor from "@uiw/react-textarea-code-editor";
// Consts
import { languagesAvailable } from '../consts/languagesAvailable';
import { levelValues } from '../consts/levelValues';

const createKataSchema = Yup.object().shape({
  name: Yup.string().required('Name is required'),
  description: Yup.string().required('Description is required'),
  level: Yup.string().required('Level is required'),
  language: Yup.string().required('Language is requierd'),
  solution: Yup.string().min(10, 'Solution too short').required('Solution is required')
})

const CreateKataForm = (props: any) => {

  const navigate = useNavigate();

  const initialValues = {
    name: '',
    description: '',
    level: '',
    language: 'js',
    solution: ''
  };

  // Formik Config
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: createKataSchema,
    onSubmit: async (values) => {
      createKata(props.token, values)
        .then((response: AxiosResponse) => {
          if (response.status === 201) {
            alert('Kata Created Successfully');
            navigate('/katas');
          } else {
            throw new Error('Error Creating Kata')
          }
        })
        .catch((error) => console.error(`[Create Kata Error]: ${error}`))
    }
  });

  return (
    <Container maxWidth='lg'>
      <CssBaseline />

      <Box
        sx={{
          margin: '2rem 0',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >

        <Typography component='h2' variant='h5'>
          Create Kata Form
        </Typography>

        <Box component='form' onSubmit={formik.handleSubmit}>
          <TextField
            required
            fullWidth
            margin='normal'
            variant='outlined'
            id='name'
            name='name'
            type='text'
            placeholder="Kata's Name"
            label="Kata's Name"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
          />

          <TextField
            required
            multiline
            fullWidth
            margin='normal'
            variant='outlined'
            id='description'
            name='description'
            type='text'
            placeholder='Write here the description'
            label='Description'
            value={formik.values.description}
            onChange={formik.handleChange}
            error={formik.touched.description && Boolean(formik.errors.description)}
            helperText={formik.touched.description && formik.errors.description}
          />

          <TextField
            required
            select
            fullWidth
            margin='normal'
            id='level'
            name='level'
            label='Level'
            value={formik.values.level}
            onChange={formik.handleChange}
            helperText='Select level'
          >
            {levelValues.map((levelValue) => (
              <MenuItem key={levelValue.value} value={levelValue.value}>
                {levelValue.label}
              </MenuItem>
            ))}
          </TextField>

          <TextField
            select
            required
            id='language'
            name='language'
            margin="normal"
            helperText="Select language"
            value={formik.values.language}
            onChange={formik.handleChange}
          >
            {languagesAvailable.map((language) => (
              <MenuItem key={language.value} value={language.value}>
                {language.label}
              </MenuItem>
            ))}
          </TextField>

          <CodeEditor
            language={formik.values.language}
            placeholder='Write kata solution here'
            value={formik.values.solution}
            onChange={formik.handleChange}
            id='solution'
            name='solution'
            padding={15}
            style={{
              fontSize: 12,
              backgroundColor: "#f5f5f5",
              fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
              width: '100%',
              height: '10rem',
              marginBottom: '2rem'
            }}
          />

          <Button variant='contained' type='submit'>Submit Kata</Button>

        </Box>
      </Box>


    </Container>
  )
}

export default CreateKataForm;