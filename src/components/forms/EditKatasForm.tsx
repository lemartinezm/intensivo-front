import { useEffect, useState } from 'react';
import { Box, Button, Container, CssBaseline, MenuItem, TextField, Typography } from '@mui/material';
import { AxiosResponse } from 'axios';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { getKataById, updateKata } from '../../services/kataService';
import { UpdateKata } from '../../utils/types/Kata.type';
// Editor
import CodeEditor from "@uiw/react-textarea-code-editor";
// Consts
import { languagesAvailable } from '../consts/languagesAvailable';
import { levelValues } from '../consts/levelValues';

const EditKatasForm = (props: any) => {

  const navigate = useNavigate();

  const [valuesLoaded, setValuesLoaded] = useState(false);

  const updateKataSchema = Yup.object().shape({
    name: Yup.string().min(10, 'Name is too short'),
    description: Yup.string().min(30, 'Description is too short'),
    level: Yup.string(),
    language: Yup.string().required('Language is requierd'),
    solution: Yup.string().min(10, 'Solution too short')
  })

  const initialValues: UpdateKata = {
    name: '',
    description: '',
    level: '',
    language: 'js',
    solution: ''
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: updateKataSchema,
    onSubmit: (values) => {
      const { name, description, level, language, solution } = values;
      const kata: UpdateKata = {}
      if (name) {
        kata.name = name;
      }
      if (description) {
        kata.description = description;
      }
      if (level) {
        kata.level = level;
      }
      if (language) {
        kata.language = language;
      }
      if (solution) {
        kata.solution = solution;
      }

      updateKata(props.loggedIn, props.id, kata)
        .then((response: AxiosResponse) => {
          if (response.status === 200) {
            alert('Kata Updated Successfully');
            navigate(-1);
          } else {
            throw new Error('Error updating kata')
          }
        })
        .catch((error) => console.error(`[Updating kata]: ${error}`))
    }
  });

  useEffect(() => {
    getKataById(props.loggedIn, props.id)
      .then(response => {
        if (response.status === 200) {
          formik.values.name = response.data.kata.name;
          formik.values.description = response.data.kata.description;
          formik.values.level = response.data.kata.level;
          formik.values.language = response.data.kata.language;
          formik.values.solution = response.data.kata.solution;
          setValuesLoaded(true);
        }
      })
      .catch(error => {
        console.error(error);
        navigate('/katas')
      })
  }, [valuesLoaded])

  return (
    <Container maxWidth='lg'>
      <CssBaseline />
      <Box
        component='main'
        sx={{
          margin: '2rem 0',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
        <Typography component='h2' variant='h5'>
          Edit Katas Form
        </Typography>

        <Box component='form' onSubmit={formik.handleSubmit}>

          <TextField
            fullWidth
            margin='normal'
            id='name'
            name='name'
            type='text'
            placeholder="Kata's Name"
            label="Kata's Name"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
          />

          <TextField
            fullWidth
            margin='normal'
            multiline
            variant='outlined'
            id='description'
            name='description'
            type='text'
            placeholder='Write here the description'
            label='Description'
            value={formik.values.description}
            onChange={formik.handleChange}
            error={formik.touched.description && Boolean(formik.errors.description)}
            helperText={formik.touched.description && formik.errors.description}
          />

          <TextField
            fullWidth
            margin='normal'
            select
            id='level'
            name='level'
            label='Level'
            value={formik.values.level}
            onChange={formik.handleChange}
            helperText='Select level'
          >
            {levelValues.map((levelValue) => (
              <MenuItem key={levelValue.value} value={levelValue.value}>
                {levelValue.label}
              </MenuItem>
            ))}
          </TextField>

          <TextField
            select
            required
            id='language'
            name='language'
            margin="normal"
            helperText="Select language"
            value={formik.values.language}
            onChange={formik.handleChange}
          >
            {languagesAvailable.map((language) => (
              <MenuItem key={language.value} value={language.value}>
                {language.label}
              </MenuItem>
            ))}
          </TextField>

          <CodeEditor
            language={formik.values.language}
            placeholder='Write kata solution here'
            value={formik.values.solution}
            onChange={formik.handleChange}
            id='solution'
            name='solution'
            padding={15}
            style={{
              fontSize: 12,
              backgroundColor: "#f5f5f5",
              fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
              width: '100%',
              height: '10rem',
              marginBottom: '2rem'
            }}
          />

          <Button type='submit' variant='contained'>Update Kata</Button>
        </Box>
      </Box>

    </Container>
  )
}

export default EditKatasForm;