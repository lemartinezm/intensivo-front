import { Routes, Route, Navigate } from "react-router-dom";
import AdminPage from "../pages/admin/AdminPage";
import HomePage from "../pages/HomePage";
import CreateKataPage from "../pages/katas/CreateKataPage";
import EditKatasPage from "../pages/katas/EditKatasPage";
import KatasDetailPage from "../pages/katas/KatasDetailPage";
import KatasPage from "../pages/katas/KatasPage";
import RegisterPage from "../pages/RegisterPage";
import KatasFromUserPage from "../pages/user/KatasFromUserPage";
import UserDetailsPage from "../pages/user/UserDetailsPage";
import UsersPage from "../pages/user/UsersPage";
import LoginPage from '../pages/LoginPage';
import AdminRegisterPage from "../pages/admin/AdminRegisterPage";

export const AppRoutes = (props: any) => {
  return (
    <Routes>
      <Route path='/' element={<HomePage />} ></Route>
      <Route path='/katas' element={<KatasPage />} ></Route>
      <Route path='/katas/:id' element={<KatasDetailPage />} ></Route>
      <Route path='/katas/create' element={<CreateKataPage />}></Route>
      <Route path='/mykatas' element={<KatasFromUserPage />} ></Route>
      <Route path='/users' element={<UsersPage />}></Route>
      <Route path='/admin/register' element={<AdminRegisterPage />}></Route>
      <Route path='/admin' element={<AdminPage isAdmin={props.isAdmin} />}></Route>
      {/* Agregadas para prueba */}
      <Route path='/users/:userId' element={<UserDetailsPage />} ></Route>
      {/*<Route path='/:userId/katas' element={<KatasFromUserPage />} ></Route>*/}
      <Route path='/katas/:id/edit' element={<EditKatasPage />}></Route>
      {/* Redirect */}
      <Route
        path='/*'
        element={<Navigate to='/' replace />} ></Route>
    </Routes>
  )
}

export const NotLoggedRoutes = () => {
  return (
    <Routes>
      <Route path='/register' element={<RegisterPage />} ></Route>
      <Route path='/*' element={<LoginPage />} ></Route>
    </Routes>
  )
}