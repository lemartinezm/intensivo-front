# Open Katas Frontend
Open Katas Frontend is a web application whose purpose is the creation and resolution of katas in diferent programming languages. The application has implemented a user registration and login option. In addition, it is possible to perform CRUD operations on users and katas (some functionalities are restricted to users with administrator role).

TypeScript, React and some other dependencies such as Material UI have been used for the project.

Realized as part of the final project proposed by OpenBootcamp in the course "Intensivo Abril - MERN"

## Usage

Clone it:

```
$ git clone https://gitlab.com/lemartinezm/intensivo-front
```

Go into the project directory and run the command:
```
npm install
```
For run:
```
npm start
```
For other scripts, check the package.json file.

## Preview
https://intensivo-front.vercel.app/

## License
[MIT](LICENSE.txt)