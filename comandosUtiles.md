# Sesión 10
Creación de la app mediante create-react-app, configuración inicial e instalación de dependencias necesarias para el proyecto front.

## Pasos
1. Crear repositorio y clonar local.
2. Instalar formik, axios, node-sass, react-router-dom y yup.
3. Crear la estructura del proyecto en src: components, pages, services y utils.
4. Crear la configuración de axios dentro de utils/config.
    * axios.create()
    * baseURL, responseType, timeout
5. Crear un authService dentro de services.
    * Debe contener los métodos de Login y Register que se usará en los componentes.
6. Crear los componentes de Login y Register dentro de la carpeta components/forms.
    * Agregar el componente a App.tsx.

# Sesión 11
Crear el enrutado para las diferentes páginas y configuración inicial sobre el acceso a determinadas páginas solo si se tiene un sessionToken almacenado en sessionStorage. Además, previa del editor de código para las katas que se terminará en las siguientes sesiones.

## Pasos:
1. Modificar el register Form para agregar una validación de la contraseña utilizando Yup.
2. Crear las diferentes páginas que se mostrarán (por ahora lo mantedremos simple mostrando solo un h1)
    * HomePage
    * KatasDetailPage
    * KatasPage
    * LoginPage
    * RegisterPage
3. Crear el enrutado de estas páginas en App.tsx
4. Agregar los componentes respectivos que debe renderizar a las páginas de registro y login.
5. Crear el hook que verifica si el usuario está loggeado (busca el JWT en SessionStorage, guardado cuando se hace un login). Especificamente el hook será useSessionStorage y buscará dentro de Session Storage por un valor mediante una key que se pasará al hook. Por ejemplo, si buscamos el token: useSessionStorage('token').
6. Permitir que solo usuarios loggeados puedan acceder a las katas y katas details.
    * Utilizar useEffect() para verificar si se tiene un sessionToken y useSessionStorage() para acceder al sessionToken
    * Dentro del katas details será necesario obtener el ID de la URL para saber que kata mostrar.
7. Crear el Editor.tsx dentro de components/editor.
    * Se utilizará prism-react-renderer. Es necesario instalar la dependencia (si no se hubiese instalado previamente).
    * Por ahora, renderizar el editor dentro de Katas Detail Page.

# Sesión 12
Implementación de petición de Katas, mostrarlas listadas en KatasPage y poder navegar a los detalles de la Kata en KataDetailPage. Configuración inicial del uso de Material UI (próximas sesiones).

## Pasos:
1. Crear el kataService. Esto controlará los pedidos al back.
    * Crear getKatas: obtiene todas las katas disponibles.
    * Crear getKataById: obtener una kata en específico.
2. Implementar dentro de KatasPage lo necesario para que muestre todas las katas de la DB.
    * Será necesario crear un type Kata.
    * Al hacer click en el título de la Kata debe mostrar una página con todos los detalles de la Kata.
3. Implementar lo necesario para que se muestre los detalles de la Kata en KataDetailPage.
4. La solución de la kata debe ser mostrada/ocultada mediante un boton en el editor de la sesión anterior.
5. Instalar lo necesario para Material UI

# Sesión 13
Simplificación de algunos componentes (separado de rutas de la app principal a un componente dentro de la carpeta rutas), creación de un Dashboard y modificación de Login y Register Form con los templates de MUI.

1. Separar el enrutado a un componente dentro de routes.
2. Crear el footer de la página.
    * Contiene el Copy y enlace a GitLab Repo.
3. Crear el menú que se mostrará dentro del Dashboard.
    * Debe contener los botones para Katas, usuarios y ranking.
    * Debe estar envuelto en un React.Fragment (sirve para inyectar código dentro de otro componente sin tener que estar creando un div u otra etiqueta)
4. Crear el Dashboard dentro de components.
    * Primero crear la AppBar.
    * shouldForwardProp: Sirve para filtrar los componentes que serán pasados como atributos. Si el prop pasado falla el test no será pasado como atributo. https://styled-components.com/docs/api. https://stackoverflow.com/questions/69730364/what-is-the-purpose-of-shouldforwardprop-option-in-styled.
    * Para dar estilos: styled(Component, [options])(styles) => Component. https://mui.com/system/styled/#styled-component-options-styles-component

# Sesión 14
Implementación del editor de código para la resolución de katas con react-simple-code-editor o tip-tap.

## Pasos:
1. Voy a probar otro editor https://uiwjs.github.io/react-textarea-code-editor/.